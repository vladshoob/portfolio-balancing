<?php

namespace App\Services\BalancingCalculator;

use App\Models\PortfolioAsset;
use App\Services\BalancingCalculator\DataTransferObjects\CalculatedPortfolioAsset;
use App\Services\BalancingCalculator\DataTransferObjects\CalculatedPortfolioAssetsCollection;
use Illuminate\Database\Eloquent\Collection;

class PreciseBalancingCalculator implements BalancingCalculatorInterface
{
    // TODO: consider making each formula a dedicated metric
    public function calculate(Collection $portfolioAssets): CalculatedPortfolioAssetsCollection
    {
        $capital = $portfolioAssets->sum(function (PortfolioAsset $portfolioAsset) {
            return $portfolioAsset->position * $portfolioAsset->asset->price;
        });

        $totalAllocation = $portfolioAssets->sum(function (PortfolioAsset $portfolioAsset) {
            return $portfolioAsset->allocation;
        });

        return new CalculatedPortfolioAssetsCollection(
            $portfolioAssets
                ->transform(function (PortfolioAsset $portfolioAsset) use ($capital, $totalAllocation) {
                    $capitalInAsset = $portfolioAsset->position * $portfolioAsset->asset->price;

                    $currentAllocation = $capitalInAsset / $capital * 100;

                    $perfectAllocation = $portfolioAsset->allocation / $totalAllocation * 100;
                    $perfectPosition = $capital * ($perfectAllocation / 100) / $portfolioAsset->asset->price;
                    $positionChangeRequired = $perfectPosition - $portfolioAsset->position;

                    return new CalculatedPortfolioAsset(
                        $portfolioAsset,
                        $currentAllocation,
                        $positionChangeRequired
                    );
                })
                ->toArray()
        );
    }
}

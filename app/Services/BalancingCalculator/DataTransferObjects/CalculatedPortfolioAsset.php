<?php

namespace App\Services\BalancingCalculator\DataTransferObjects;

use App\Models\PortfolioAsset;

class CalculatedPortfolioAsset
{
    public function __construct(
        public PortfolioAsset $portfolioAsset,
        public float $currentAllocation,
        public float $changeRequired
    ) {}
}

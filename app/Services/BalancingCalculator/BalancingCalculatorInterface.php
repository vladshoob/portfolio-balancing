<?php

namespace App\Services\BalancingCalculator;

use App\Services\BalancingCalculator\DataTransferObjects\CalculatedPortfolioAssetsCollection;
use Illuminate\Database\Eloquent\Collection;

interface BalancingCalculatorInterface
{
    public function calculate(Collection $portfolioAssets): CalculatedPortfolioAssetsCollection;
}

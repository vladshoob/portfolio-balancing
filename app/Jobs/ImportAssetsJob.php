<?php

namespace App\Jobs;

use App\Models\Asset;
use App\Repositories\AssetsRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Scheb\YahooFinanceApi\ApiClientFactory;

class ImportAssetsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private const ASSET_NAMES = [
        'VOO',
        'IJR',
        'IJH',
        'VNQ',
        'VEA',
        'VWO',
    ];

    public function handle(AssetsRepository $assetsRepository): void
    {
        $client = ApiClientFactory::createApiClient();

        foreach (self::ASSET_NAMES as $symbol) {
            $quote = $client->getQuote($symbol);

            if (!$quote) {
                Log::warning(sprintf('No data found for `%s` symbol', $symbol));
            }

            $asset = $assetsRepository->findOneWithSymbol($symbol);

            if (!$asset) {
                $asset = new Asset();
                $asset->symbol = $symbol;
            }

            $asset->price = round($quote->getRegularMarketPreviousClose() * 100);

            $assetsRepository->save($asset);
        }
    }
}

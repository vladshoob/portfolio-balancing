<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePortfolioAssetRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'symbol' => 'required|string|max:4|exists:assets,symbol',
            'position' => 'required|numeric|min:0',
            'allocation' => 'required|numeric|min:0|max:100',
        ];
    }
}

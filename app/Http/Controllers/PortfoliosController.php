<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePortfolioRequest;
use App\Models\Portfolio;
use App\Repositories\PortfoliosRepository;
use Illuminate\Database\Eloquent\Collection;

class PortfoliosController extends Controller
{
    public function index(PortfoliosRepository $portfoliosRepository): Collection
    {
        return $portfoliosRepository->all();
    }

    public function store(StorePortfolioRequest $request, PortfoliosRepository $portfoliosRepository): Portfolio
    {
        $portfolio = new Portfolio($request->validated());

        $portfoliosRepository->save($portfolio);

        return $portfolio;
    }
}

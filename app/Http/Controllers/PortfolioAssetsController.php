<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePortfolioAssetRequest;
use App\Models\PortfolioAsset;
use App\Models\Portfolio;
use App\Repositories\AssetsRepository;
use App\Repositories\PortfolioAssetsRepository;
use App\Services\BalancingCalculator\DataTransferObjects\CalculatedPortfolioAssetsCollection;
use App\Services\BalancingCalculator\PreciseBalancingCalculator;

class PortfolioAssetsController extends Controller
{
    public function index(
        Portfolio $portfolio,
        PortfolioAssetsRepository $assetsRepository,
        PreciseBalancingCalculator $balancingCalculator
    ): CalculatedPortfolioAssetsCollection {
        // TODO: fix user & portfolio indifference
        $portfolioAssets = $assetsRepository->all()
            ->load('asset');

        return $balancingCalculator->calculate($portfolioAssets);
    }

    public function store(
        Portfolio $portfolio,
        StorePortfolioAssetRequest $request,
        AssetsRepository $assetsRepository,
        PortfolioAssetsRepository $portfolioAssetsRepository
    ): PortfolioAsset {
        $portfolioAsset = new PortfolioAsset($request->only(['position', 'allocation']));
        $portfolioAsset->setPortfolio($portfolio);
        $portfolioAsset->setAsset($assetsRepository->findOneWithSymbol($request->get('symbol')));

        $portfolioAssetsRepository->save($portfolioAsset);

        return $portfolioAsset;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $symbol
 * @property int $price
 */
class Asset extends Model
{
    use HasFactory;

    protected $fillable = ['symbol', 'price'];
}

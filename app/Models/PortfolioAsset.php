<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $position
 * @property int $allocation
 * @property Asset $asset
 */
class PortfolioAsset extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'position', 'allocation'];

    public function portfolio(): BelongsTo
    {
        return $this->belongsTo(Portfolio::class);
    }

    public function setPortfolio(Portfolio $portfolio): self
    {
        return $this->portfolio()->associate($portfolio);
    }

    public function asset(): BelongsTo
    {
        return $this->belongsTo(Asset::class);
    }

    public function setAsset(Asset $asset): self
    {
        return $this->asset()->associate($asset);
    }
}

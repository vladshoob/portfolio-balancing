<?php

namespace App\Repositories;

use App\Models\PortfolioAsset;

class PortfolioAssetsRepository extends BaseRepository
{
    public function __construct(private PortfolioAsset $model)
    {
        parent::__construct($model);
    }
}

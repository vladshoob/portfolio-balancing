<?php

namespace App\Repositories;

use App\Models\Asset;

class AssetsRepository extends BaseRepository
{
    public function __construct(private Asset $model)
    {
        parent::__construct($model);
    }

    public function findOneWithSymbol(string $symbol): ?Asset
    {
        return $this->model->newQuery()
            ->where('symbol', $symbol)
            ->first();
    }
}

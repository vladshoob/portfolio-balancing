<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    public function __construct(private Model $model) {}

    public function all(): Collection
    {
        return $this->model->newQuery()->get();
    }

    public function save(Model $model): Model
    {
        $model->save();

        return $model;
    }
}

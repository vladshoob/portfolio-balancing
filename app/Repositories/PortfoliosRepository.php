<?php

namespace App\Repositories;

use App\Models\Portfolio;

class PortfoliosRepository extends BaseRepository
{
    public function __construct(private Portfolio $model)
    {
        parent::__construct($model);
    }
}

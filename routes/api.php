<?php

use App\Http\Controllers\PortfolioAssetsController;
use App\Http\Controllers\PortfoliosController;
use Illuminate\Support\Facades\Route;

//Route::middleware('auth:api')->group(function () {

    Route::prefix('portfolios')->group(function () {
        Route::resource('/', PortfoliosController::class)->only(['index', 'store']);

        Route::prefix('{portfolio}')->group(function () {
            Route::resource('assets', PortfolioAssetsController::class)->only(['index', 'store']);
        });
    });

//});

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfolioAssetsTable extends Migration
{
    public function up(): void
    {
        Schema::create('portfolio_assets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('portfolio_id');
            $table->unsignedBigInteger('asset_id');
            $table->unsignedInteger('position');
            $table->unsignedInteger('allocation');
            $table->timestamps();

            $table->foreign('asset_id')->references('id')->on('assets');
            $table->foreign('portfolio_id')->references('id')->on('portfolios');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('portfolio_assets');
    }
}

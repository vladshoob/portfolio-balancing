<?php

namespace Database\Seeders;

use App\Jobs\ImportAssetsJob;
use App\Models\Asset;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        // TODO: might have its own class

        Asset::factory()->create([
            'symbol' => 'Cash',
            'price' => 100,
        ]);

        ImportAssetsJob::dispatchSync();
    }
}
